package game;

/**
 * creating the interface for bullet.
 */
public interface BulletInterface {

    /**
     * returns the owner of the bullet.
     * @return owner of the bullet.
     */
    ID getOwner();

}

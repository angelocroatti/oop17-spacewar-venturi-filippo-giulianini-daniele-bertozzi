package game.obstacles;

import game.GameObject;

/**
 * Interface used to divide work with team-mates.
 */
public interface Obstacle extends GameObject {
}

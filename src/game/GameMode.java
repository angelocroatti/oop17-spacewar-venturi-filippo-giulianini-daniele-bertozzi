package game;

/**
 * An enum describing the possible game modes.
 */
public enum GameMode {
    /**
     * Singleplayer mode.
     */
    SINGLEPLAYER,

    /**
     * Multiplayer mode.
     */
    MULTIPLAYER;
}
